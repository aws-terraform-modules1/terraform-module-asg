variable "asg_name" {
  type        = string
  description = "(string, Required) name of the ASG"
}

variable "max_size" {
  type = number
  description = "(int, Required) maximum number of instances you want"
}

variable "min_size" {
  type = number
  description = "(int, Required) minimum number of instances you want"
}

variable "health_check_grace_period" {
  type = number
  description = "(int, Required) time between health checks (in seconds)"
}

variable "desired_capacity" {
  type = number
  description = "(int, Required) the prefered number of instances"
}

variable "ami" {
  type = string 
  description = "(string, Required) the ec2 ami"
}

variable "instance_type" {
  type = string
  description = "(string, Required) t2.micro or whatever ec2 instance type you want"
}

variable "subnets" {
  type = list(string)
  description = "(list(string), Required) list of subnets (at least 2)"
}

variable "elb_id" {
  type = string
  description = "(string, Required) id of the loadbalancer"
}