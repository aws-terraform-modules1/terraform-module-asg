resource "aws_autoscaling_group" "Bozo" {
  name                      = var.asg_name
  max_size                  = var.max_size
  min_size                  = var.min_size
  health_check_grace_period = var.health_check_grace_period
  health_check_type         = "ELB"
  desired_capacity          = var.desired_capacity
  force_delete              = true
  placement_group           = aws_placement_group.pg.id
  vpc_zone_identifier       = var.subnets

  tag {
    key                 = "bingus"
    value               = "yep"
    propagate_at_launch = true
  }
  launch_template {
    id      = aws_launch_template.balling.id
  }
}

resource "aws_placement_group" "pg" {
  name     = "amogus"
  strategy = "cluster"
}

resource "aws_launch_template" "balling" {
  name_prefix   = "buck-buck"
  image_id      = var.ami
  instance_type = var.instance_type
}

resource "aws_autoscaling_attachment" "asg_attachment" {
  autoscaling_group_name = aws_autoscaling_group.Bozo.id
  elb                    = var.elb_id
}